
# Master Thesis
This projects contains the code used to write my master thesis "Indirect Forecasting of Power Generation From a PV Power Plant Through Data-Driven and Physical Methods". No data is included for privacy reasons. The code is organiszed in four folders as follows.

* Preprossesing
* Orion files
* Evaluation
* Physical method

## Preprossesing
This folders cointains the preprosessing steps used for all the data as well as the concatination of the data from different scources.

## Orion files
The porsess for Method 1 is included in this file. The data driven method for GHI is roughly equal. The hyperparameter combinations were made with make_set and make_combinations and made the comb and set files. Combinations from these files were used as inputs in the "power" python scripts who evaluated every combination with an RFR model. To iterate over all the combinations with several evaluations running in parallell, the slurm files were used. These initiated every single evaluation and fed them with the right parameter combination from the set and comb files. All the results were saved in the O_P directories and RFR_read_out found the best combinations in each step of the method. 

## Physical model
The power generation is calcultated with help of a series of pvlib functions.

## Evaluation
The best RFR model found in the orion files is used to make the forecasts for Method 1. All the methods are evaluated with PAPE, MBE, RMSE, MSE and skill score. 

## Vizualization
This file makes some vizualizations that were used in the thesis. 


