# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 15:01:18 2023

@author: Sigrid
"""
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import timezone
import numpy as np
import missingno as msno
#%%
met = pd.read_csv('met_forecast.csv')
met = met.rename(columns = {'index': 'time'})
met['time'] = pd.to_datetime(met.time, dayfirst = True)
met = met.set_index('time')

#%%
measure = pd.read_csv('measure_full.csv')
measure['time'] = pd.to_datetime(measure.time, dayfirst = True)

measure = measure.set_index('time')

#%% Concatninate data
full = pd.concat([met, measure], axis = 1)
#full = full.index.replace(tzinfo=None)
#%%
full.to_csv('total_hour.csv')
full[:int(len(full)*0.66)].to_csv('train_full.csv')

