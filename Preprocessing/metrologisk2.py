# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 09:48:17 2023

@author: Sigrid
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import missingno as msno
from sklearn.metrics import mean_squared_error

#%%
met_data = pd.read_csv('weather_data_apodi.csv')

met_data.forecast_from = pd.to_datetime(met_data.forecast_from, dayfirst= True)
met_data = met_data.set_index('forecast_from')
met_data = met_data.loc[met_data.index >= '2020-01-01 00:00:00+00:00']


met_1 = pd.read_csv('apodi_12_hours_ahead.csv')
met_1.forecast_from = pd.to_datetime(met_1.forecast_from, dayfirst= True)
met_data = met_data.tz_convert(None)
#%% Change the column names
met_frame = met_data.copy(deep=True)
for column in met_data.columns:
    new = column.rsplit(' ', 1)[0]
    if new != 'forecast_hours_ahead':
        new = new.replace(' ', '_')
        new = new + '_pred'
        met_frame = met_frame.rename(columns={column: new})
        

#%% Make predictions start at 0000 and 24 hours ahead
mask1 = met_frame.index.hour == met_frame.forecast_hours_ahead
mask2 = met_frame.index.hour == 0 
mask3 = met_frame.forecast_hours_ahead == 24
data1 = met_frame[mask1]
data23 = met_frame[mask2 & mask3]
hour = pd.concat([data1, data23], axis = 0)


hour = hour.sort_index(ascending = True)  




#%%
new_time = pd.date_range(start='2020-01-01 00:00:00', end='2022-12-30 23:45:00', freq = '1H')
met_full = hour[~hour.index.duplicated()]
met_full = met_full.reindex(new_time).reset_index()
met_full = met_full.set_index('index')


#%% Make 48 hour forecsts to use for imputing

mask1 = met_frame.index.hour +24== met_frame.forecast_hours_ahead 
mask2 = met_frame.index.hour == 0 
mask3 = met_frame.forecast_hours_ahead == 48
data1 = met_frame[mask1]
data23 = met_frame[mask2 & mask3]
hour48 = pd.concat([data1, data23], axis = 0)
mask = hour48.index.hour + hour48.forecast_hours_ahead == 24
hour48 = hour48[~mask]

hour48 = hour48.sort_index(ascending = True)  

#%%
new_time = pd.date_range(start='2020-01-01 00:00:00', end='2022-12-30 23:45:00', freq = '1H')
met_48 = hour48[~hour48.index.duplicated()]
met_48 = met_48.reindex(new_time).reset_index()
met_48 = met_48.set_index('index')

#%% Make continus forecast for different  forecast horizons
def make_continous_forecast(horizon, met_frame = met_frame):
    if 24%horizon != 0:
        raise ValueError('24 %  horizon must be zero')
    mask  = met_frame.forecast_hours_ahead <= horizon
    data1 = met_frame[mask]
    data = []
    for i in range(int(24/horizon)):
        mask = data1.index.hour == data1.forecast_hours_ahead + horizon * i
        data.append(data1[mask])
        
    mask = data1.index.hour == 0
    midnight = data1[mask]
    mask = midnight.forecast_hours_ahead == horizon
    midnight = midnight[mask]
    data.append(midnight)
    cont = pd.concat(data, axis = 0)
    cont = cont.sort_index(ascending = True) 
    
    new_time = pd.date_range(start='2020-01-01 00:00:00', end='2022-12-30 23:45:00', freq = '1H')
    met = cont[~cont.index.duplicated()]
    met = met.reindex(new_time).reset_index()
    met = met.set_index('index')
    return met
#%% 4 hours ahead forecast
met_4 = make_continous_forecast(4)
met_8 = make_continous_forecast(8)
#%%Visualize
plt.figure(figsize = (15,5))
met_full.Temperature_pred[-240:].plot(label = '24h horizon')
met_48.Temperature_pred[-240:].plot(label = '48h horizon')
met_4.Temperature_pred[-240:].plot(label = '4h horizon')
plt.title('Predictions from midnigth - 24h ahead?')
plt.legend()

#%%
diff = met_full- met_48
diff_stats = diff.describe()

#%% Plotter ting jeg ikke skjønner helt....
plt.figure(figsize = (15,5))
for i in range(24):
    mask1 = met_frame.index.hour ==0 
    mask2 = met_frame.forecast_hours_ahead == i 
    mask  = mask1 & mask2
    hour = met_frame.loc[mask]
    #hour = hour.set_index(newtime)
    lab = 'ahead' + str(i)
    hour.Temperature_pred.plot(label = lab)
plt.legend( loc = 'center right', bbox_to_anchor=(1.22,0.5))
plt.title('Change forecast_hours_ahead')
plt.tight_layout()

plt.figure(figsize = (15,5))
for i in range(24):
    mask1 = met_frame.index.hour == i
    mask2 = met_frame.forecast_hours_ahead == 1 
    mask  = mask1 & mask2
    hour = met_frame.loc[mask]
    #hour = hour.set_index(newtime)
    lab = 'from' + str(i)
    hour.Temperature_pred.plot(label = lab)
plt.legend( loc = 'center right', bbox_to_anchor=(1.22,0.5))
plt.title('Change forecast_from')
plt.tight_layout()

#%% Impute with 48 hour forecast
def impute_other_horizon(dataset, replacement):
    mask = dataset[24:].isnull().any(axis=1)
    mask = dataset[24:].loc[mask].index
    met_imput = dataset.copy(deep = True)
    n=0
    for time in mask:
        if replacement.loc[time].isnull().any() == False:
            met_imput.loc[time][1:] = replacement.loc[time][1:]
            n +=1 
    print(n)
    print(n/len(met_full))
    return met_imput

#%% Check nigthtime values
def impute_night(dataset):
    mask = dataset.isnull().any(axis = 1)
    mask = dataset.loc[mask].index
    met_imput_night = dataset.copy(deep = True)
    Mean = dataset.mean(axis = 0)
    n = 0
    
    for time in mask:
        if time.hour < 7 or time.hour > 23:
            met_imput_night.loc[time][1:] =  Mean[1:]
            n+=1
    print(n)
    print(n/len(met_full))
    return met_imput_night


#%% Count consecutive missing values
def count_consecutive(dataframe):
    mask = dataframe.Temperature_pred.isnull()
    mask = dataframe.loc[mask].index
    consecutive = []
    n = 0
    
    for i, indeks in enumerate(mask[:-1]):
        if indeks + dt.timedelta(hours = 1) == mask[i+1]: # If given index is followed by the next, it continues counting
            n +=1
        else:                       # If given indes is not followed by the next, the values is appended and count start over
            n += 1
            consecutive.append(n)
            n = 0
    return consecutive

#%% Fill inn small holes
def impute_small_holes(dataframe, hole_size = 4):
    met_imputed = dataframe.copy(deep = True)
    holes = count_consecutive(met_imputed)
    mask = met_imputed.isnull().any(axis=1)
    # Fill in holes 

    h= 1
    n=0
    total_count = 0
    x = 1
    hole_size = hole_size
    for indeks in met_imputed.loc[mask].index: 
        if x == 1 :
            x =0
        elif holes[h] < hole_size:
            total_count +=1
            n += 1
            met_imputed.loc[indeks] = met_imputed.loc[indeks- dt.timedelta(hours = n)].copy(deep = True)
            if met_imputed.loc[indeks].isnull().sum() != 0:
                print('something strange', holes[h], n, indeks)
                print(met_full.loc[indeks- dt.timedelta(hours =n+2)])
            if n == holes[h]:
                h+=1
                n=0
        else:
            n += 1
            if n == holes[h]:
                h+=1
                n=0
        if h == len(holes):
            break
    print(total_count)
    print(total_count/len(met_full))
    return met_imputed

#%% 
met_imp_48 = impute_other_horizon(met_full, met_48)
#met_4_imp_8 = impute_other_horizon(met_4, met_8)
#%%
met_imp_nigth = impute_night(met_imp_48)
#met_4_imp_night = impute_night(met_4_imp_8)
#%%
met_imp_small = impute_small_holes(met_imp_48)
#met_4_imp_small = impute_small_holes(met_4_imp_night)
    
#%%
met_full.isnull().sum()/len(met_full)
#%% Save data

#met_imp_small[24:].to_csv('met_forecast.csv')
#met_4_imp_small[24:].to_csv('met_4_forecast.csv')
#%%
mask = met_full.isnull().any(axis=1)
mask = list(~np.array(mask))
mask = met_full[mask].index
met_c = met_full.loc[mask]
met_48_c = met_48.loc[mask]
#%%
mask = met_48_c.isnull().any(axis=1)
mask = list(~np.array(mask))
mask = met_48_c[mask].index
met_clean = met_c.loc[mask]
met_48_clean = met_48_c.loc[mask]
met_clean = met_clean.sort_index(ascending = True)  
met_46_clean = met_48_clean.sort_index(ascending = True)  

rmse = mean_squared_error(met_clean.Precipitation_pred,met_48_clean.Precipitation_pred, squared = False)

#%%
plt.figure(figsize = (8.5,2))
mask = met_full.Precipitation_pred.isnull()
plt.plot(met_imp_48.index[mask], np.zeros(mask.sum()),'x', label = 'Rain before')
mask = met_full.isnull().all(axis=1)
plt.plot(met_imp_48.index[mask], np.zeros(mask.sum()),'x', label = 'All before ')
mask = met_imp_48.isnull().all(axis=1)
plt.plot(met_imp_48.index[mask], np.zeros(mask.sum()),'x', label = 'After imputation')
plt.legend(ncols = 3)
plt.title('Missing values in yr weather forecast')
plt.yticks([], [])
plt.tight_layout()
plt.savefig('Figure/missing_yr.jpg', dpi = 600)