# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 10:24:13 2023

@author: Sigrid
"""
import pandas as pd
import numpy as np 

from boruta import BorutaPy

from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
# %% Import data

data = pd.read_csv('train_full.csv')
data.time = pd.to_datetime(data.time, dayfirst=True)
data = data.set_index('time')


data = data.dropna(axis=0)
data = data.drop(columns = ['hour', 'month', 'year', 'pressure', 'rain', 'forecast_hours_ahead', 
                            'produced', 'day', 'tracker','incline', '24h_delay_i',
                           'ambient', 'wind_direction', 'wind_speed', 'module',
                           'elevation'])
#data = data.drop(columns = ['hour', 'month', 'year', 'pressure', 'rain', 'forecast_hours_ahead', 
 #                           'day','incline', '24h_delay_i',
  #                          'ambient', 'wind_direction', 'wind_speed', 'module',
   #                         'elevation'])


#%%

data = pd.read_csv('train_w_GHIpred.csv')
data.time = pd.to_datetime(data.time, dayfirst=True)
data = data.set_index('time')


data = data.drop(columns = ['hour', 'month', 'year', 'pressure', 'rain', 'forecast_hours_ahead', 
                           'day','incline', '24h_delay_i', 'produced',
                           'ambient', 'wind_direction', 'wind_speed', 'module',
                           'elevation', 'horizontal'])
n = len(data)
splitt = 0.66
data = data.dropna(axis=0)
data = data[:int(0.66*0.66*n)]

# %% Scale data



scaler = MinMaxScaler()
# Scale only training data later
scaler.fit(data[:int(n*splitt)])

scaled = scaler.transform(data)
data_sc = data.copy()
data_sc.iloc[:,:] = scaled
data_sc = data_sc.sort_index(ascending=True)
#y = data_sc.pop('horizontal')
y = data_sc.pop('produced_fixed')
#%%
ranking = []
for i in range(10):
    X_train, X_test, y_train, y_test = train_test_split(data_sc, y, test_size = 0.33, random_state=i)

    forest = RandomForestRegressor(n_jobs=-1)

    forest.fit(X_train, y_train)
    forest.feature_importances_

    
    # define Boruta feature selection method
    feat_selector = BorutaPy(forest, n_estimators='auto', verbose=2, random_state=i)
    
    # find all relevant features
    feat_selector.fit(np.array(X_train), np.array(y_train))
    
    ranking.append(feat_selector.ranking_)
 
#%%
rank_df = pd.DataFrame(data=ranking, columns = data_sc.columns)
rank_df.to_csv('rank_future_power.csv')