# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 10:42:35 2023

@author: Sigrid
"""
from sklearn.inspection import permutation_importance
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
import math
import scipy
import darts
import time 

from brokenaxes import brokenaxes

from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split

# Darts
from darts import TimeSeries
from darts.models import RNNModel, ExponentialSmoothing, BlockRNNModel
from darts.metrics import mape, rmse
from darts.utils.statistics import check_seasonality, plot_acf
from darts.datasets import AirPassengersDataset, SunspotsDataset
from darts.utils.timeseries_generation import datetime_attribute_timeseries
from darts.models.forecasting.random_forest import RandomForest

import ipywidgets as ipw
import hvplot.xarray # noqa
import hvplot.pandas # noqa
import panel as pn
import pandas as pd
import panel.widgets as pnw
import xarray as xr

# %% Import data
start = time.time()
data = pd.read_csv('full_w_all_pred.csv')

data.time = pd.to_datetime(data.time, dayfirst=True)
data = data.set_index('time')
train_split = 16728
test_split = 20544
data['24h_delay_p'] = data.produced_fixed.shift(24)


#%%
##no_miss = data.iloc[maske]

#no_miss_train = no_miss[test_split:]
# %%
solar = pd.read_csv('solar.csv')
solar.time = pd.to_datetime(solar.time, dayfirst=True)
solar = solar.set_index('time')
solar = solar.resample('H', label='right', closed='right').mean()

solar = solar[25:]
#solar = solar[:int(len(solar)*0.66)]
sol_max = solar.produced_pred[:test_split].max()

# %%
solar.produced_pred = solar.produced_pred/sol_max
solar.produced_pred['2022-07-27 01:00:00':'2022-07-28 23:00:00'] = np.nan


# %% Scale data
n = len(data)
splitt = 0.66

scaler = MinMaxScaler()
# Scale only training data later
scaler.fit(data[:train_split])

scaled = scaler.transform(data)
data_sc = data.copy()
data_sc.iloc[:,:] = scaled
data_sc = data_sc.sort_index(ascending=True)

# %% Splitt data
past = TimeSeries.from_dataframe(
    data_sc[['ambient', 'wind_direction']].copy(deep=True))
future = TimeSeries.from_dataframe(data_sc[['Medium_clouds_pred', 'clear_sky', '24h_delay_h', 'Wind_Direction_pred', 'Humidity_pred', 'horizontal_pred']].copy(deep=True))
                                           
#y      = TimeSeries.from_series(data_sc.iloc[:,incline_full])
y = TimeSeries.from_series(data_sc[['produced_fixed']])

# %%

train_past, val_past = past.split_before(train_split)
train_future, val_future = future.split_before(train_split)
train_y, val_y = y.split_before(train_split)
#%% Make mask for days with nan
def mask_days_with_nan(data, solar):
    periods = int(len(solar)/24)
    indexes = np.linspace(0, periods-1, periods,
                          dtype=int)
    mask = []
    
    for i in indexes:

        if data[i*24:i*24+24].isnull().sum().sum() + solar.iloc[i*24:i*24+24].isnull().sum()==0:
            mask.extend(np.arange(i*24,i*24+24,1))
        else:
            print('moe')
    return mask
futuretest = data[['Medium_clouds_pred', 'clear_sky', '24h_delay_h', 'Wind_Direction_pred', 'Humidity_pred', 'horizontal_pred', 'produced_fixed', '24h_delay_p']].copy(deep=True)
futuretest = futuretest[test_split:]
maske = mask_days_with_nan(futuretest, solar[test_split:].produced_pred)
#%% Comparing metrics
solar_test =solar[test_split:]#[-8928:]
solar_test = solar_test.iloc[maske]
persist_test = data_sc['24h_delay_p'][test_split:]#[-8928:]
persist_test = persist_test.iloc[maske]
y_test = y.values()[test_split:]
y_test = y_test[maske]
#%%
solar_test.produced_pred.plot()
plt.plot(solar_test.index,y_test)
#%%


# %% Make model
def make_fit_model(rs=44):
    rfr_darts = RandomForest(lags=48,
                             lags_future_covariates=[24, 24],
                             output_chunk_length=24, n_estimators=900, max_features = 2,
                             random_state=(rs))

    
    
    rfr_darts.fit(train_y,  future_covariates=train_future,
                  n_jobs_multioutput_wrapper=None, max_samples_per_ts=10000)
    
    return rfr_darts
#%%       
rfr_darts = make_fit_model()
#%%
#different_ts = pd.DataFrame(data=liste, columns = ['num', 'tid','mse'])
#plt.plot(different_ts.tid, different_ts.mse, '*', color ='g')
#for i, txt in enumerate(different_ts.num):
#    plt.annotate(txt, (different_ts.tid[i], different_ts.mse[i]))
#%%
plt.title('Computation time and accuracy for different max_samples_per_ts')
plt.xlabel('Time [s]')
plt.ylabel('MSE')
# %%


def PAPE(truth, pred, horizon):
    tot = 0
    periods = int(len(truth) / horizon)
    indexes = np.linspace(0, periods*horizon-1, periods,
                          dtype=int, endpoint=False)
    for i in indexes:
        truth_peak = max(truth[i:i+horizon])
        pred_peak = max(pred[i:i+horizon])
        if truth_peak != 0:
            tot += abs(truth_peak - pred_peak)/truth_peak
    return float((100/periods)*tot)


# %% Visualize


class predictions:
    def __init__(self, model):
        self.model = model

    def predict(self, y, past, future, persistence, horizon=96, num_pred=6, splitt=None, visualize=True):
        self.splitt = test_split

        self.num_pred = int(np.floor(len(data[test_split:])/horizon))
        self.horizon = int(horizon)
        self.prediction = []
        self.truth = []
        self.solar =[]
        self.emp =[]
        self.persist = []
        self.ss_val = []
        values = []
        if visualize:
            fig, ax = plt.subplots(3, sharex=True, figsize=(8.5, 7))
            fig2, ax2 = plt.subplots(1)

        for i in range(self.num_pred):
            ind = self.splitt + i * self.horizon -1
            if future[ind:ind+2*horizon].pd_dataframe().isnull().sum().sum()  + y[ind-2*horizon:ind+horizon].pd_dataframe().isnull().sum().sum() + solar.produced_pred.iloc[ind: ind + horizon].isnull().sum() == 0:
                past_covariates = past[:ind]
                history_y = y[:ind]
                true_y = y[ind:]
    
                darts_pred = self.model.predict(n=horizon, series=history_y, 
                                                future_covariates=future)
                
                #weight = piv.produced.iloc[data.hour[ind:ind+self.horizon]]
                #pred_w = mean_squared_error(true_y[:self.horizon].values(
                # , darts_pred.values(), squared=False, sample_weight=weight)
                pred = mean_squared_error(true_y[:self.horizon].values(), 
                                          darts_pred.values())
                pers = mean_squared_error(true_y[:self.horizon].values(), 
                                          data_sc[persistence].iloc[ind: ind + self.horizon])
                GIS = mean_squared_error(true_y[:self.horizon].values(), 
                                         solar.produced_pred.iloc[ind: ind + self.horizon])
                emp = mean_squared_error(true_y[:self.horizon].values(),
                                         data_sc.empiric.iloc[ind:ind+horizon])
                pape_p = PAPE(true_y[:self.horizon].values(),
                              darts_pred.values(), horizon)
                pape_s = PAPE(true_y[:self.horizon].values(
                ), solar.produced_pred.iloc[ind: ind + self.horizon], horizon)
                ss_pred = 1 - (math.sqrt(pred)/math.sqrt(pers))
                ss_GIS = 1 - (math.sqrt(GIS)/math.sqrt(pers))
                ss_emp = 1 - (math.sqrt(emp)/math.sqrt(pers))
                
                values.append([pred,  pers, GIS, ss_pred, ss_GIS, pape_p, pape_s])
                self.ss_val.append([ss_pred, ss_GIS, ss_emp])
                self.prediction.extend(np.stack(darts_pred.values(), axis=1)[0])
                self.truth.extend(np.stack(true_y[:horizon].values(), axis=1)[0])
                self.solar.extend(solar.produced_pred.iloc[ind: ind + self.horizon])
                self.emp.extend(data_sc.empiric.iloc[ind:ind+horizon])
                self.persist.extend(data_sc[persistence].iloc[ind: ind + self.horizon])
                if visualize:
            
                    ax[0].plot(data[ind:ind+horizon].index,
                               true_y[:horizon].values(), color='black', ls='-')
                    ax[0].plot(data[ind:ind+horizon].index,
                               data_sc[persistence].iloc[ind: ind + self.horizon], color='g', ls=':')
                    ax[0].plot(data[ind:ind+horizon].index,
                               solar.produced_pred.iloc[ind:ind+horizon], color='blue', ls='--')
                    ax[0].plot(data[ind:ind+horizon].index,
                               darts_pred.values(), color='r', ls='-.')
                    ax[0].plot(data[ind:ind+horizon].index,
                               data_sc.empiric.iloc[ind:ind+horizon], color='purple', linestyle = (0, (3, 1, 1, 1, 1, 1)))
                    
                   
                    ax[1].plot(data[ind:ind+horizon].index, true_y[:horizon].values() -
                               darts_pred.values(), color='r', ls='-.')
                    ax[1].plot(data[ind:ind+horizon].index, np.stack(true_y[:horizon].values(), axis=1)[0] -
                               data_sc[persistence].iloc[ind: ind + self.horizon], color='g', ls=':')
                    ax[1].plot(data[ind:ind+horizon].index, np.stack(true_y[:horizon].values(), axis=1)[0]
                               - solar.produced_pred.iloc[ind:ind+horizon], color='blue', ls='--')
                    ax[1].plot(data[ind:ind+horizon].index, np.stack(true_y[:horizon].values(), axis=1)[0]
                               - data_sc.empiric.iloc[ind:ind+horizon], color='purple', linestyle = (0, (3, 1, 1, 1, 1, 1)))
                    
    
                    ax[2].scatter(data.iloc[ind+12].name, ss_pred, marker = '*', color = 'r')
                    ax[2].scatter(data.iloc[ind+12].name, ss_GIS, marker = 'x',color= 'b')
                    ax[2].scatter(data.iloc[ind+12].name, ss_emp, marker = 'o',color= 'purple', facecolors='none')
                    #fig.legend(['Predictions', 'SolarGIS'],bbox_to_anchor=(1.12, 0.7))
                    ax[2].set_ylim(-2,1)

                    #ax2.plot(data.hour.iloc[ind:ind +
                            # self.horizon], darts_pred.values())
                    #ax2.plot(data.hour.iloc[ind:ind+self.horizon],
                             #true_y[:horizon].values(), ls=':')
                    
            
            else:
                print('It skipped....')
        
        ax[0].set_title('Forecasts')
        ax[1].set_title('Error')
        ax[2].set_title('Skill score')
        ax[0].set_ylabel(r'Power generation [$\emptyset$]')
        ax[1].set_ylabel(r'Power deviation [$\emptyset$]')
        ax[2].set_ylabel(r'Skill score [$\emptyset$]')
        ax[0].legend(['True',   'Smart persistence', 'Solargis', 'Method 1', 'Method 2'],
                      bbox_to_anchor=(0.5, -2.8), ncols = 5, loc = 'center')
        fig.align_ylabels()
        plt.tight_layout
        

        self.mean = mean_squared_error(self.truth, self.prediction)
        self.pape = PAPE(self.truth, self.prediction, horizon)
        self.ss = pd.DataFrame(self.ss_val, columns = ['Pred', 'Solar', 'emp'])
        self.rmse = pd.DataFrame(values, columns=[
                                 'Predictions', 'Persistence', 'SolarGIS', 
                                 'SkillScore_pred', 'SkillScore_GIS', 
                                 'Pape_p', 'Pape_s'])
        self.rmse_tot = self.rmse.mean(axis=0, numeric_only=True)
        self.rmse_tot.SkillScore_pred = 1 - \
           (self.rmse_tot.Predictions / self.rmse_tot.Persistence)
        self.rmse_tot.SkillScore_GIS = 1 - \
            (self.rmse_tot.SolarGIS / self.rmse_tot.Persistence)
        

#https://hvplot.holoviz.org/user_guide/Interactive.html
# %% Predict
prede = predictions(rfr_darts)
prede.predict(y=y, past = past, future=future, splitt=0.66, num_pred=350,
             horizon=24, visualize=True, persistence = '24h_delay_p'
             )

prede.rmse_tot


#%%

#%%
ss = pd.DataFrame(prede.ss_val, columns = ['Pred', 'Solar', 'emp'])
fig, ax = plt.subplots(1,figsize = (8.5,5))
#baxes = brokenaxes(xlims=((-180, -160),(-27,1)))

sns.kdeplot(ax = ax, x=prede.ss.Solar, bw_adjust=.1,  label ='Solargis', color ='blue', ls='--', clip=(-3,1.5))
sns.kdeplot(ax = ax, x=prede.ss.Pred, bw_adjust=.1, label = 'Method 1', color = 'red', ls='-.', clip=(-3,1.5))
sns.kdeplot(ax = ax, x=prede.ss.emp, bw_adjust=.1,  label = 'Method 2', color ='purple', linestyle = (0, (3, 1, 1, 1, 1, 1)), clip=(-3,1.5))
plt.title('Distribution for skill scores')
ax.set_xlabel('Skill score')
plt.legend()
plt.savefig('Figure/skillscore.jpg', bbox_inches="tight", dpi=600)
#%%
print('solar:', prede.ss.Solar.min(), 'num_min_10:', len(prede.ss.Solar[prede.ss.Solar<-3]))
print('data:', prede.ss.Pred.min(), 'num_min_10:', len(prede.ss.Pred[prede.ss.Pred<-3]))
print('emp:', prede.ss.emp.min(),'num_min_10:', len(prede.ss.emp[prede.ss.emp<-3]))
#%%
fig,ax = plt.subplots(ncols=4,nrows=1, sharey = True, sharex=True, figsize = (8.5,3))
ax[0].scatter(prede.truth, prede.solar, color='b', marker = 'x',linewidth =1,s=10)
ax[0].plot(prede.truth,prede.truth, color='black')
ax[0].set_title('Solargis')
ax[1].scatter(prede.truth, prede.prediction,  color='r', marker = '*', linewidth =0.5, s=10)
ax[1].plot(prede.truth,prede.truth, color='black')
ax[1].set_title('Method 1')
ax[2].scatter(prede.truth, prede.emp, color='purple', marker = 'o', facecolors = 'none', linewidth =1, s=10)
ax[2].plot(prede.truth,prede.truth, color='black')
ax[2].set_title('Method 2')
ax[3].scatter(prede.truth, prede.persist, color='green', marker = '.', facecolors = 'none', linewidth =1, s=10)
ax[3].plot(prede.truth,prede.truth, color='black')
ax[3].set_title('Smart persistence')
ax[0].set_ylabel('Forecasted generation')
#fig.supxlabel('True generation')

fig.text(0.5, -0.01, 'True generation', ha='center',weight='bold')
plt.tight_layout()
plt.savefig('Figure/compare.jpg', bbox_inches="tight", dpi=600)
# %%
print(prede.rmse.Pape_p.mean())
print(prede.pape)
#print(PAPE(pred.true, pred.prediction))

print(prede.rmse.Predictions.mean())
print(prede.mean)


# %% Evaluate


class evaluation:
    def __init__(self, model):
        self.model = model

    def evaluate(self, y, past, future, horizon=24):
        
        # Sett number of predictions to num whole prediction horizons in val set
        num_pred = int(np.floor(len(data[test_split:])/horizon))
        self.prediction = []
        truth = []
        val_start = len(y) - num_pred * horizon  # Where the validation starts
        n = 0
        m = 0
        # Make the predictions
        for i in range(num_pred):
            ind = test_split + i * horizon - 1
            # Only run prediction if there are 0 nan values in horizon
            if future[ind:ind+2*horizon].pd_dataframe().isnull().sum().sum()  + y[ind-2*horizon:ind+horizon].pd_dataframe().isnull().sum().sum() + solar.produced_pred.iloc[ind: ind + horizon].isnull().sum() == 0:
                n += 1
                #print(n)
                past_covariates = past[:ind]
                history_y = y[:ind]
                true_y = y[ind:]
                

                darts_pred = self.model.predict(n=horizon, series=history_y, 
                                                future_covariates=future)

                self.prediction.extend(np.stack(darts_pred.values(), axis=1)[0])
                truth.extend(np.stack(true_y[:horizon].values(), axis=1)[0])
                
            else:
                m += 1
                #print('nan', m)
                
        print('predicting done', n, 'days predicted')
        self.pape = PAPE(truth, self.prediction, horizon)
        self.mse = mean_squared_error(truth, self.prediction)
        self.mbe = np.mean([true - pred for (true, pred) in zip(truth, self.prediction)])

# %%
pred = evaluation(rfr_darts)
pred.evaluate(y=y, past=past, future=future, horizon =24)
#%%
rmse_persistence = math.sqrt(mean_squared_error(prede.truth, prede.persist))
pape_per = PAPE(prede.truth, prede.persist,24)
mse_per  = mean_squared_error(prede.truth, prede.persist)
#ss_per = 1 - math.sqrt(mse_solar)/rmse_persistence
MBE_per = np.mean([true - pred for (true, pred) in zip(prede.truth, prede.persist)])

pape_solar = PAPE(prede.truth, prede.solar,24)
mse_solar  = mean_squared_error(prede.truth, prede.solar)
ss_solar = 1 - math.sqrt(mse_solar)/rmse_persistence
MBE_solar  = np.mean([true - pred for (true, pred) in zip(prede.truth, prede.solar)])

ss_pred = 1 - math.sqrt(pred.mse) / rmse_persistence
print('RMSE pred: ',math.sqrt(pred.mse))
print('PAPE pred :', pred.pape)
print('MBE  pred  :', pred.mbe)
print('MSE  pred:',  pred.mse)
print('SS   pred:', ss_pred)

print('MBE  solar :',  MBE_solar)
print('RMSE solar:',math.sqrt(mse_solar))
print('PAPE solar:',pape_solar)
print('MSE  solar:',mse_solar)
print('SS   solar:',ss_solar)

print('RMSE pers:', math.sqrt(mse_per))
print('PAPE pers:', pape_per)
print('MBE  pers:',  MBE_per)
print('MSE  pers:',  mse_per)
print('SS   pers:', 0)

pape_emp = PAPE(prede.truth, prede.emp,24)
mse_emp  = mean_squared_error(prede.truth, prede.emp)
ss_emp = 1 - math.sqrt(mse_emp)/rmse_persistence
MBE_emp  = np.mean([true - pred for (true, pred) in zip(prede.truth, prede.emp)])

print('RMSE emp:',math.sqrt(mse_emp))
print('PAPE emp:',pape_emp)
print('MBE  emp:',  MBE_emp)
print('ss   emp:',  ss_emp)
print('MSE  emp:',  mse_emp)
 

# %% Get mean metrics
import random
random.seed(2)
values = []
for i in range(10):
    rs = random.randint(1,1000)
    rfr_darts = make_fit_model(rs)
    pred = evaluation(rfr_darts)
    pred.evaluate(y=y, past=past, future=future, horizon =24)
    values.append([pred.mbe,  pred.mse, pred.pape])
#%%
results = pd.DataFrame(values, columns=['mbe', 'mse', 'pape'])
print('MBE:', results.mbe.mean())
print('MSE:', results.mse.mean())
print('RMSE:', math.sqrt(results.mse.mean()))
print('PAPE:', results.pape.mean())
print('Skill score:', 1 - math.sqrt(results.mse.mean()) / rmse_persistence)






