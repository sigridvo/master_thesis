# -*- coding: utf-8 -*-
"""
Created on Tue Apr 11 15:35:06 2023

@author: Sigrid
"""
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
import pvlib
import math
from pvlib  import solarposition, tracking, pvsystem
from pvlib.irradiance import clearness_index, get_extra_radiation
import missingno as msno
import matplotlib.patches as mpatches

#%%
data = pd.read_csv('full_w_GHIpred.csv')
data.time = pd.to_datetime(data.time, dayfirst=True)
data = data.set_index('time')
solar = pd.read_csv('solar.csv')
solar.time = pd.to_datetime(solar.time, dayfirst=True)
solar = solar.set_index('time')
#%% Clearness index
times    = pd.date_range('2020-01-02 01:00', '2022-12-31 00:00:00', freq='1H', tz='Etc/GMT+0')
times = times.tz_localize(None)
solar_position = solarposition.spa_python(times, latitude=-5.044958, longitude=-37.793078)
extra = get_extra_radiation(times)
#%%
maske= data.elevation > 0
CI = clearness_index(ghi = data.horizontal[maske], solar_zenith = solar_position.zenith[maske], 
                     extra_radiation = extra, max_clearness_index=1)
plt.figure(figsize = (8.5,5))
plt.hist(CI, bins=200, color = 'navy')
plt.grid(False)
plt.title('Clearness index distribution')
sns.displot( CI, kde = True)
#%%
plt.figure()
data.horizontal.plot()
plt.figure()
extra.plot()
#%% Comparison of dataset
train_split = 16728
test_split = 20544
train_h = data[:train_split]
val_h   = data[train_split:test_split]
test_h  = data[test_split:]



CI_train = clearness_index(ghi = train_h.horizontal[maske],
                           solar_zenith = solar_position.zenith[:train_split], 
                           extra_radiation = extra[:train_split][maske[:train_split]], 
                           max_clearness_index=0.9)
CI_val   = clearness_index(ghi = val_h.horizontal[maske],
                           solar_zenith = solar_position.zenith[train_split:test_split], 
                           extra_radiation = extra[train_split:test_split][maske[train_split:test_split]], 
                           max_clearness_index=0.9)
CI_test  = clearness_index(ghi = test_h.horizontal[maske],
                           solar_zenith = solar_position.zenith[test_split:], 
                           extra_radiation = extra[test_split:][maske[test_split:]], 
                           max_clearness_index=0.9)

fig,ax = plt.subplots(3,figsize = (8.5,5), sharex = True)
ax[0].hist(CI_train, bins=200, color = 'navy')
ax[0].set_title('Clearness index train set')
ax[1].hist(CI_val, bins=200, color = 'red')
ax[1].set_title('Clearness index validation set')
ax[2].hist(CI_test, bins=200, color = 'blue')
ax[2].set_title('Clearness index test set')
plt.tight_layout()


#%%kde plot

fig,ax = plt.subplots(1,figsize = (8.5,3), sharex = True)
#ax[0].hist(CI_train, bins=200, color = 'navy')

sns.kdeplot(ax=ax, x=CI_train, bw_adjust=.2, linestyle = '-.')
sns.kdeplot(ax=ax, x=CI_val, bw_adjust=.2, linestyle = ':')
sns.kdeplot(ax=ax, x=CI_test, bw_adjust=.2, linestyle="--")
sns.kdeplot(ax=ax, x=CI, bw_adjust=.2, color = 'r')
ax.set_xlabel('Clearness Index')
plt.title('Clearness index for split dataset')
plt.tight_layout()


ax.legend(['training data',  'validation data','test data', 'all data'],
              bbox_to_anchor=(1, 1))
plt.savefig('Figure/CI.jpg',bbox_inches="tight", dpi=600)
#%% mean and sd
train_sd = train_h.horizontal.std()
val_sd   = val_h.horizontal.std()
test_sd  = test_h.horizontal.std()

train_mean = train_h.horizontal.mean()
val_mean   = val_h.horizontal.mean()
test_mean  = test_h.horizontal.mean()

print('train', train_sd, train_mean)
print('val  ', val_sd, val_mean)
print('test ', test_sd, test_mean)


#%% plot all variables over days
mask = (data.index >= '2020-01-02 00:00') & (data.index <= '2022-12-31 00:00')

fig,ax = plt.subplots(8, figsize =(8.5,8), sharex=True)
ax[0].plot(data.index[mask], data.horizontal.loc[mask])
ax[0].set_ylabel(r'GHI [$\frac{W}{m^2}$]')
ax[1].plot(data.index[mask], data.ambient.loc[mask])
ax[1].set_ylabel(r'$T_{amb}$ [$^\circ C$]')
ax[2].plot(data.index[mask], data.module.loc[mask])
ax[2].set_ylabel(r'$T_{mod}$ [$ ^\circ C$]')
ax[3].plot(data.index[mask], data.wind_direction.loc[mask])
ax[3].set_ylabel(r'$W_{dir}$ [$^\circ $]')
ax[4].plot(data.index[mask], data.wind_speed.loc[mask])
ax[4].set_ylabel(r'$W_{speed}$ [$\frac{m}{s}$]')
ax[5].plot(data.index[mask], data.pressure.loc[mask])
ax[5].set_ylabel(r'P [$hPa$]')
ax[5].set_ylim(970,1010)
ax[6].plot(data.index[mask], data.rain.loc[mask])
ax[6].set_ylabel(r'PCPN [$mm$]')
ax[7].plot(data.index[mask], data.produced.loc[mask])
ax[7].set_ylabel(r'P [$W$]')
ax[7].set_ylim(0,0.5*10**8)
ax[7].set_yticks([])
fig.subplots_adjust(hspace=0)
#plt.tight_layout()
fig.align_ylabels()

#%% Compare datasets w/ power split

train_split = 16728
test_split = 20544


#%%
plt.figure()
plt.plot(solar.index, solar.produced_pred*200, label = 'solar')
plt.plot(data.index, data.produced_fixed, color = 'r', label = 'true')
plt.legend()

#%%
times_15 = pd.date_range('2020-01-02 07:00', '2020-01-02 22:00:00', freq='15T', tz='Etc/GMT+0')
solar_position = solarposition.spa_python(times_15, latitude=-5.044958, longitude=-37.793078)
angles_b = tracking.singleaxis(apparent_zenith = solar_position.apparent_zenith, 
                          apparent_azimuth = solar_position.azimuth, axis_tilt=0, axis_azimuth=0, max_angle=60,
                             backtrack=True, gcr=3.93/14)
angles = tracking.singleaxis(apparent_zenith = solar_position.apparent_zenith, 
                          apparent_azimuth = solar_position.azimuth, axis_tilt=0, axis_azimuth=0, max_angle=60,
                             backtrack=False, gcr=3.93/14)
angles_m = tracking.singleaxis(apparent_zenith = solar_position.apparent_zenith, 
                          apparent_azimuth = solar_position.azimuth, axis_tilt=0, axis_azimuth=0,
                             backtrack=False, gcr=3.93/14)
plt.figure(figsize = (8.5,2.5))
angles_m.surface_tilt.plot(label = 'No regulation' , color = 'salmon')
angles.surface_tilt.plot(label = 'Max tilt angle ' ,color = 'cornflowerblue',ls='--')
angles_b.surface_tilt.plot(label = 'Backtracking and max tilt angle', color = 'limegreen', ls =':')
#data.elevation[8:21].plot(label = 'Elevation')

plt.ylabel(r'Tilt angle [$^\circ$]')
plt.xlabel(r'Time')
plt.legend(loc = 'center',bbox_to_anchor =(0.5,1.1), ncols =3)
plt.tight_layout()
plt.savefig('Figure/backtracking.jpg')


#%% plot all yr variables over days
mask = (data.index >= '2022-07-29 00:00') & (data.index <= '2022-08-05 00:00')

fig,ax = plt.subplots(8, figsize =(8.5,9), sharex=True)
ax[0].plot(data.index[mask], data.Humidity_pred.loc[mask])
ax[0].set_title('Humidity')
ax[0].set_ylabel(r'$\%$')
ax[1].plot(data.index[mask], data.Temperature_pred.loc[mask])
ax[1].set_title('Ambient temperature')
ax[1].set_ylabel(r'$^\circ C$')
ax[2].plot(data.index[mask], data.Dewpoint_temperature_pred.loc[mask])
ax[2].set_title('')
ax[2].set_ylabel(r'$ ^\circ C$')
ax[3].plot(data.index[mask], data.Wind_Direction_pred.loc[mask])
ax[3].set_title('Wind direction')
ax[3].set_ylabel(r'$Azimuth ^\circ $')
ax[4].plot(data.index[mask], data.Wind_speed_pred.loc[mask])
ax[4].set_title('Wind speed')
ax[4].set_ylabel(r'$\frac{m}{s}$',size =14)
ax[5].plot(data.index[mask], data.Pressure_pred.loc[mask])
ax[5].set_title('Pressure')
ax[5].set_ylabel(r'$hPa$')
ax[6].plot(data.index[mask], data.Precipitation_pred.loc[mask])
ax[6].set_title('Precipitation')
ax[6].set_ylabel(r'$mm$')
ax[7].plot(data.index[mask], data.Clouds_pred.loc[mask], label = 'Total', linewidth = 4, color = 'black')
ax[7].plot(data.index[mask], data.Low_Clouds_pred.loc[mask], label = 'Low clouds')
ax[7].plot(data.index[mask], data.Medium_clouds_pred.loc[mask],label = 'Medium clouds')
ax[7].plot(data.index[mask], data.High_clouds_pred.loc[mask], label = 'High clouds')
ax[7].legend(loc='center', bbox_to_anchor = (0.5, -0.8), ncols=4)
ax[7].set_title('Cloud cover')
ax[7].set_ylabel(r'$\%$')
plt.tight_layout()
fig.align_ylabels()
plt.show()

#%%
solar2 = solar.resample('H', label='right', closed='right').mean()
plt.figure()
plt.plot(data.index, data.produced_fixed/data.produced_fixed.max(), label = 'pred')
plt.plot(solar.index, solar.produced/solar.produced.max(), label = 'solar', ls = '-.')
plt.plot(solar2.index, solar2.produced/solar2.produced.max(), label = 'solar H', ls = ':')
plt.legend()
#%%

plt.figure()
plt.plot(data.index, data.horizontal_pred, label ='pred')
plt.plot(solar.index, solar.horizontal_pred, label = 'solar', ls = '-.')
plt.plot(solar2.index, solar2.horizontal_pred, label = 'solar H', ls = ':')
plt.legend()

#%% Heatmap
mask = data.elevation > 0
data_day = data[mask]
important = data_day[['horizontal','produced_fixed','ambient', 'module', 'wind_direction', 
                  'wind_speed', 'pressure', 'rain', 'Precipitation_pred',
                  'Humidity_pred','Temperature_pred', 'Dewpoint_temperature_pred',
                  'Wind_speed_pred', 'Wind_Direction_pred', 'Pressure_pred',
                  'Clouds_pred','Low_Clouds_pred', 'Medium_clouds_pred', 'High_clouds_pred',
                  ]]

plt.figure(figsize = (8.5,6))
sns.heatmap(important.corr()*100, annot = True,fmt=".0f", linewidth=.5)
plt.tight_layout()
plt.savefig('Figure/corr.jpg',bbox_inches="tight", dpi=600)


#%%

plt.figure()
plt.plot(data.pressure, data.Pressure_pred, '*')
#%% Visualize missing days
#%% Make mask for days with nan

data2 = data[['Medium_clouds_pred', 'clear_sky', '24h_delay_h', 'Wind_Direction_pred', 'Humidity_pred', 'horizontal_pred', 'produced_fixed']]
data3 = data[['ambient', 'wind_direction', 'Temperature_pred','Clouds_pred','Low_Clouds_pred','Medium_clouds_pred','24h_delay_h','Precipitation_pred', 'horizontal']]
def mask_days_with_nan(data):
    periods = int(len(data)/24)
    indexes = np.linspace(0, periods-1, periods,
                          dtype=int)
    mask = []
    
    for i in indexes:

        if data.iloc[i*24:i*24+24].isnull().sum().sum() ==0:
            mask.extend(np.arange(i*24,i*24+24,1))
        else:
            print('moe')
    return mask
maske2 = mask_days_with_nan(data2)
maske3 = mask_days_with_nan(data3)
#%%
def plot_days_with_nan(data1, data2):
    
    periods = int(len(data)/24)
    indexes = np.linspace(0, periods-1, periods,
                          dtype=int)
    mask = []
    
    for i in indexes:
        if data1.iloc[i*24:i*24+24].isnull().sum().sum() !=0:
             plt.scatter(data2.index[i*24],0,marker ='|', color = 'cornflowerblue', s = 150)
        if data2.iloc[i*24:i*24+24].isnull().sum().sum() !=0:
             plt.scatter(data2.index[i*24],0,marker ='|', color = 'blue', s = 100 )

plt.figure(figsize = (8.5,2))
plot_days_with_nan(data2, data3)
#plt.title('Missing days')
red= mpatches.Patch(color='cornflowerblue', label='Missing days for power forecast')
blue = mpatches.Patch(color='blue', label='Missing days for GHI forecast')
plt.legend(handles =[red,blue],loc='center', bbox_to_anchor = (0.5, 1.2), ncols=2)
plt.yticks([])
plt.xlabel('Time')
plt.tight_layout()
plt.savefig('Figure/missing.jpg',bbox_inches="tight", dpi=600)
#%% 
plt.figure
plt.plot(data2.iloc[maske2].index, [1]*len(maske2), 'x', color='black', markersize = 10)

plt.plot(data3.iloc[maske3].index, [1]*len(maske3), 'x', color = 'green')
frac_missing3 = (len(data3)-len(data2.iloc[maske3]))/len(data2)
print(frac_missing2, frac_missing3)

#%%
#https://pvlib-python.readthedocs.io/en/v0.9.0/auto_examples/plot_singlediode.html

n = 1
T = 300
Il = 0.5
I0 = 1*10**(-10)
k =  1.380649 * 10**(-23)
q = 1.602176634 * 10**(-19)
Rs = 1.065
R_sh_ref = 381.68
T_NOCT = 42.4,
I= np.arange(0,0.5,0.001)

V = []
for i in I:
    V.append(((n*k*T)/q)*math.log((Il-i)/I0))
    #I.append(Il-I0*(math.e**((q*v)/n*k*T)))
I = np.append(I, 0.5)
V.append(0)

Il = 0.35
I2= np.arange(0,0.35,0.001)
V2 = []
for i in I2:
    V2.append(((n*k*T)/q)*math.log((Il-i)/I0))
    #I.append(Il-I0*(math.e**((q*v)/n*k*T)))
I2 = np.append(I2, 0.35)
V2.append(0)

T=275
Il = 0.5
I3= np.arange(0,0.5,0.001)
V3 = []
for i in I3:
    V3.append(((n*k*T)/q)*math.log((Il-i)/I0))
    #I.append(Il-I0*(math.e**((q*v)/n*k*T)))
I3 = np.append(I3, 0.5)
V3.append(0)

curve_info = pvsystem.singlediode(
    photocurrent=Il,
    saturation_current=I0,
    resistance_series=Rs,
    resistance_shunt=Rsh,
    nNsVth=nNsVth,
    ivcurve_pnts=100,
    method='lambertw'
)

fig,ax = plt.subplots(1, figsize = (8.5,3.5))
ax.plot(V,I, label = 'Current', color = 'salmon')
ax.plot(V2,I2, label = 'Current reduced irradiance', color = 'limegreen', ls =':')
#ax.plot(V, I*V, label = 'Power')
#ax.plot(V2, I2*V2, label = 'Power reduced irradiance')
ax.plot(V3, I3, label = 'Current increased temperature', color = 'cornflowerblue', ls='--')
plt.legend()
ax.set_xlabel('Voltage [V]')
ax.set_ylabel('Current [A]')
plt.title('IV curves for PV cells')
plt.tight_layout()
#%%

from pvlib import pvsystem
import pandas as pd
import matplotlib.pyplot as plt

# Example module parameters for the Canadian Solar CS5P-220M:
parameters = {
    'Name': 'Canadian Solar CS5P-220M',
    'BIPV': 'N',
    'Date': '10/5/2009',
    'T_NOCT': 42.4,
    'A_c': 1.7,
    'N_s': 96,
    'I_sc_ref': 5.1,
    'V_oc_ref': 59.4,
    'I_mp_ref': 4.69,
    'V_mp_ref': 46.9,
    'alpha_sc': 0.004539,
    'beta_oc': -0.22216,
    'a_ref': 2.6373,
    'I_L_ref': 5.114,
    'I_o_ref': 8.196e-10,
    'R_s': 1.065,
    'R_sh_ref': 381.68,
    'Adjust': 8.7,
    'gamma_r': -0.476,
    'Version': 'MM106',
    'PTC': 200.1,
    'Technology': 'Mono-c-Si',
}

cases = [
    (1000, 25, 'Standard Testing Conditions', 'salmon', '-'),
    (800, 25, 'Reduced irradiance', 'limegreen', ':'),
    (1000, 55, 'Increased temperature', 'cornflowerblue', '--'),

]

conditions = pd.DataFrame(cases, columns=['Geff', 'Tcell', 'lable', 'color', 'style'])

# adjust the reference parameters according to the operating
# conditions using the De Soto model:
IL, I0, Rs, Rsh, nNsVth = pvsystem.calcparams_desoto(
    conditions['Geff'],
    conditions['Tcell'],
    alpha_sc=parameters['alpha_sc'],
    a_ref=parameters['a_ref'],
    I_L_ref=parameters['I_L_ref'],
    I_o_ref=parameters['I_o_ref'],
    R_sh_ref=parameters['R_sh_ref'],
    R_s=parameters['R_s'],
    EgRef=1.121,
    dEgdT=-0.0002677
)

# plug the parameters into the SDE and solve for IV curves:
curve_info = pvsystem.singlediode(
    photocurrent=IL,
    saturation_current=I0,
    resistance_series=Rs,
    resistance_shunt=Rsh,
    nNsVth=nNsVth,
    ivcurve_pnts=100,
    method='lambertw'
)

# plot the calculated curves:
plt.figure(figsize = (8.5,3))
for i, case in conditions.iterrows():
    label = (cases[i][2])
    plt.plot(curve_info['v'][i], curve_info['i'][i], label=label, 
             color = cases[i][3], ls = cases[i][4])
    v_mp = curve_info['v_mp'][i]
    i_mp = curve_info['i_mp'][i]
    # mark the MPP
    plt.plot([v_mp], [i_mp], ls='', marker='o', c=cases[i][3])

plt.legend()
plt.xlabel('Voltage [V]')
plt.ylabel('Current [A]')
plt.title('IV curves for PV cells')
plt.xticks([])
plt.yticks([])
plt.tight_layout()
plt.savefig('Figure/IV.jpg',bbox_inches="tight", dpi=600)

#%%
mask = (data.index >= '2022-08-01 00:00') & (data.index <= '2022-08-08 00:00')

fig,ax = plt.subplots(15, figsize =(8.5,12.5), sharex=True)
ax[0].plot(data.index[mask], data.horizontal.loc[mask], color = 'black')
ax[0].set_title('Measured GHI')
ax[0].set_ylabel(r'$\frac{W}{m^2}$',size =14)
ax[1].plot(data.index[mask], data.ambient.loc[mask], color = 'black')
ax[1].set_title('Measured ambient temperature')
ax[1].set_ylabel(r'$^\circ C$')
ax[2].plot(data.index[mask], data.module.loc[mask], color = 'black')
ax[2].set_title('Measured module temperature')
ax[2].set_ylabel(r'$ ^\circ C$')
ax[3].plot(data.index[mask], data.wind_direction.loc[mask], color = 'black')
ax[3].set_title('Measured wind direction')
ax[3].set_ylabel(r'$Azimuth ^\circ $')
ax[4].plot(data.index[mask], data.wind_speed.loc[mask], color = 'black')
ax[4].set_title('Measured wind speed')
ax[4].set_ylabel(r'$\frac{m}{s}$',size =14)
ax[5].plot(data.index[mask], data.pressure.loc[mask], color = 'black')
ax[5].set_title('Measured pressure')
ax[5].set_ylabel(r'$hPa$')
ax[6].plot(data.index[mask], data.rain.loc[mask], color = 'black')
ax[6].set_title('Measured precipitation')
ax[6].set_ylabel(r'$mm$')
ax[7].plot(data.index[mask], data.Humidity_pred.loc[mask])
ax[7].set_title('Predicted humidity')
ax[7].set_ylabel(r'$\%$')
ax[8].plot(data.index[mask], data.Temperature_pred.loc[mask])
ax[8].set_title('Predicted ambient temperature')
ax[8].set_ylabel(r'$^\circ C$')
ax[9].plot(data.index[mask], data.Dewpoint_temperature_pred.loc[mask])
ax[9].set_title('Predicted dewpoint temperature')
ax[9].set_ylabel(r'$ ^\circ C$')
ax[10].plot(data.index[mask], data.Wind_Direction_pred.loc[mask])
ax[10].set_title('Predicted wind direction')
ax[10].set_ylabel(r'$Azimuth ^\circ $')
ax[11].plot(data.index[mask], data.Wind_speed_pred.loc[mask])
ax[11].set_title('Predicted wind speed')
ax[11].set_ylabel(r'$\frac{m}{s}$',size =14)
ax[12].plot(data.index[mask], data.Pressure_pred.loc[mask])
ax[12].set_title('Predicted pressure')
ax[12].set_ylabel(r'$hPa$')
ax[13].plot(data.index[mask], data.Precipitation_pred.loc[mask])
ax[13].set_title('Predicted precipitation')
ax[13].set_ylabel(r'$mm$')
ax[14].plot(data.index[mask], data.Clouds_pred.loc[mask], label = 'Total', linewidth = 4, color = 'black')
ax[14].plot(data.index[mask], data.Low_Clouds_pred.loc[mask], label = 'Low clouds', color = 'salmon')
ax[14].plot(data.index[mask], data.Medium_clouds_pred.loc[mask],label = 'Medium clouds', color ='limegreen')
ax[14].plot(data.index[mask], data.High_clouds_pred.loc[mask], label = 'High clouds', color = 'gold')
ax[14].legend(loc='center', bbox_to_anchor = (0.5, -1.2), ncols=4)
ax[14].set_title('Predicted cloud cover')
ax[14].set_ylabel(r'$\%$')

plt.tight_layout()
fig.align_ylabels()


plt.savefig('Figure/everything.jpg',bbox_inches="tight", dpi=1200)

#%%
mask = (data.index >= '2022-08-01 00:00') & (data.index <= '2022-08-08 00:00')

fig,ax = plt.subplots(10, figsize =(8.5,10), sharex=True)
ax[0].plot(data.index[mask], data.horizontal.loc[mask], color = 'cornflowerblue')
ax[0].set_title('GHI', loc='left')
ax[0].set_ylabel(r'$\frac{W}{m^2}$',size =14)
ax[1].plot(data.index[mask], data.module.loc[mask], color = 'cornflowerblue')
ax[1].set_title('Module temperature', loc='left')
ax[1].set_ylabel(r'$ ^\circ C$')
ax[2].plot(data.index[mask], data.ambient.loc[mask], color = 'cornflowerblue', label ='Measurement')
ax[2].plot(data.index[mask], data.Temperature_pred.loc[mask], color = 'black', ls ='--', label= 'Forecast')
#ax[2].legend(loc='center', bbox_to_anchor = (0.5, -9), ncols=2)
ax[2].set_title('Ambient temperature', loc='left')
ax[2].set_ylabel(r'$^\circ C$')
ax[3].plot(data.index[mask], data.wind_direction.loc[mask], color = 'cornflowerblue',)
ax[3].plot(data.index[mask], data.Wind_Direction_pred.loc[mask], color = 'black', ls ='--')
ax[3].set_title('Wind direction', loc='left')
ax[3].set_ylabel(r'$Azimuth ^\circ $')
ax[4].plot(data.index[mask], data.wind_speed.loc[mask], color = 'cornflowerblue',)
ax[4].plot(data.index[mask], data.Wind_speed_pred.loc[mask], color = 'black', ls ='--')
ax[4].set_title('Wind speed', loc='left')
ax[4].set_ylabel(r'$\frac{m}{s}$',size =14)
ax[5].plot(data.index[mask], data.pressure.loc[mask], color = 'cornflowerblue',)
ax[5].plot(data.index[mask], data.Pressure_pred.loc[mask], color = 'black',  ls ='--')
ax[5].set_title('Pressure', loc='left')
ax[5].set_ylabel(r'$hPa$')
ax[6].plot(data.index[mask], data.Precipitation_pred.loc[mask], color = 'black',  ls ='--')
ax[6].plot(data.index[mask], data.rain.loc[mask], color = 'cornflowerblue')
ax[6].set_title('Precipitation', loc='left')
ax[6].set_ylabel(r'$mm$')
ax[7].plot(data.index[mask], data.Humidity_pred.loc[mask], color = 'black',ls ='--')
ax[7].set_title('Humidity', loc='left')
ax[7].set_ylabel(r'$\%$')
ax[8].plot(data.index[mask], data.Dewpoint_temperature_pred.loc[mask], color = 'black',ls ='--')
ax[8].set_title('Dewpoint temperature', loc='left')
ax[8].set_ylabel(r'$ ^\circ C$')
ax[9].plot(data.index[mask], data.Clouds_pred.loc[mask], label = 'Total', linewidth = 4, color = 'black')
ax[9].plot(data.index[mask], data.Low_Clouds_pred.loc[mask], label = 'Low clouds', color = 'salmon', ls = '--')
ax[9].plot(data.index[mask], data.Medium_clouds_pred.loc[mask],label = 'Medium clouds', color ='gold',linestyle = (0, (3, 1, 1, 1, 1, 1)))
ax[9].plot(data.index[mask], data.High_clouds_pred.loc[mask], label = 'High clouds', color = 'limegreen', ls = ':')
#ax[9].legend(loc='center', bbox_to_anchor = (0.5, -1.2), ncols=4)
ax[9].set_title('Cloud cover', loc='left')
ax[9].set_ylabel(r'$\%$')
handles2, labels2 = ax[2].get_legend_handles_labels()
handles9, labels9 = ax[9].get_legend_handles_labels()
plt.tight_layout()
fig.legend(handles2, labels2, loc='center right',bbox_to_anchor = (0.98, 0.98), ncols=2)
fig.legend(handles9, labels9, loc='center right',bbox_to_anchor = (0.98, -0.00), ncols=4)
fig.align_ylabels()
fig.grid(axis='x')
plt.savefig('Figure/everything2.jpg',bbox_inches="tight", dpi=600)


