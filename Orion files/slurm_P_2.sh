#!/bin/bash
#SBATCH --job-name=RFR_array2
#SBATCH --ntasks=10              # 1 core(CPU)
#SBATCH --nodes=1                 # Use 1 node
#SBATCH --job-name=RFR_H      # sensible name for the job
#SBATCH --mem=3G                 # Default memory per CPU is 3GB.
#SBATCH --partition=smallmem     # Use the smallmem-partition for jobs requiring < 10 GB RAM.
#SBATCH --output=O_P_2/log_%a.out
#SBATCH --array=100001-100100%10

#Specify the path to file with combinations
combinations='comb_P_2.txt'

past=$(awk -v ArrayTaskID=$SLURM_ARRAY_TASK_ID '$1==ArrayTaskID {print $2}' $combinations)
future=$(awk -v ArrayTaskID=$SLURM_ARRAY_TASK_ID '$1==ArrayTaskID {print $3}' $combinations)

module load singularity

exec python Power_2.py ${past} ${future} 


