import numpy             as np
import pandas            as pd
import sys
from sklearn.metrics                        import mean_squared_error
from sklearn.preprocessing                  import MinMaxScaler
from darts                                  import TimeSeries
from darts.models.forecasting.random_forest import RandomForest


past       = int(sys.argv[1])
future     = int(sys.argv[2])

data = pd.read_csv('train_w_GHIpred.csv')
data['time'] = pd.to_datetime(data.time, dayfirst= True)
data = data.set_index('time')


scaler = MinMaxScaler()
scaler.fit(data[:16728])
scaled = scaler.transform(data)
data_sc = data.copy()
data_sc.iloc[:,:] = scaled
data_sc = data_sc.sort_index(ascending=True)

#%% Make different data combinations
past_variables   = pd.read_csv('comb_past_P.csv')
future_variables = pd.read_csv('comb_future_P.csv')
past_possible     = [x for x in past_variables.loc[past] if x == x]
future_possible   = [x for x in future_variables.loc[future] if x == x]

#%% Splitt data 
past   = TimeSeries.from_dataframe(data_sc[past_possible].copy(deep = True))

future = TimeSeries.from_dataframe(data_sc[future_possible].copy(deep= True))
y = TimeSeries.from_series(data_sc[['produced_fixed']])

train_past, val_past     = past.split_before(16728)
train_future, val_future = future.split_before(16728)
train_y, val_y           = y.split_before(16728)



rfr_darts = RandomForest(lags=24, 
                         lags_future_covariates = [24,24],
                         lags_past_covariates = [-24],
                         output_chunk_length=24, n_estimators=1000)

rfr_darts.fit(train_y, future_covariates=train_future, past_covariates = 
              train_past, max_samples_per_ts=3000,
              n_jobs_multioutput_wrapper=None)

def PAPE(truth, pred, horizon):
    tot = 0
    periods = int(len(truth) / horizon)
    indexes = np.linspace(0, periods*horizon-1, periods,
                          dtype=int, endpoint=False)
    for i in indexes:
        truth_peak = max(truth[i:i+horizon])
        pred_peak = max(pred[i:i+horizon])
        tot += abs(truth_peak - pred_peak)/truth_peak
    return float((100/periods)*tot)

#%% Evalueate
def evaluate(model, y, past, future):
    horizon = 24
    #Sett number of predictions to num whole prediction horizons in val set
    num_pred = int(np.floor(len(val_y)/horizon))
    prediction = []
    truth = []
    val_start = len(y) - num_pred * horizon # Where the validation starts
    n=0
    m=0
    #Make the predictions 
    for i in range(num_pred-2):
        ind = val_start + i * horizon -1
        #Only run prediction if there are 0 nan values in horizon
        if future[ind:ind+2*horizon].pd_dataframe().isnull().sum().sum()  + y[ind-       2*horizon:ind+horizon].pd_dataframe().isnull().sum().sum() == 0:
            n+=1
            past_covariates = past[:ind]
            history_y       = y[:ind]
            true_y          = y[ind:]

            darts_pred =model.predict(n = horizon, series = history_y, 
                                      future_covariates = future, past_covariates = past_covariates)
            prediction.extend(np.stack(darts_pred.values(), axis=1)[0])
            truth.extend(np.stack(true_y[:horizon].values(),axis=1)[0])
    

    pape = PAPE(truth, prediction, horizon) 
    mse  = mean_squared_error(truth, prediction)
    mbe = np.mean([true - pred for (true, pred) in zip(truth, prediction)])

    return(pape, mse, mbe)

pape, mse, mbe = evaluate(rfr_darts, y=y, past=past, future = future)
print(pape, mse, mbe)
print(past_possible)
print(future_possible)

 