# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 13:00:56 2023

@author: Sigrid
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
import math
import pvlib

from sklearn.metrics import mean_squared_error

from pvlib import clearsky, atmosphere, solarposition, tracking, location
#%% Mask days with nan
def mask_days_with_nan(data, solar):
    periods = int(len(data)/24)
    indexes = np.linspace(0, periods-1, periods,
                          dtype=int)
    mask = []
    
    for i in indexes:

        if data.iloc[i*24:i*24+24].isnull().sum().sum() + solar.iloc[i*24:i*24+24].isnull().sum()==0:
            mask.extend(np.arange(i*24,i*24+24,1))
        else:
            print('moe')
    return mask
#%% Import data
data = pd.read_csv('full_w_GHIpred.csv')
data['time'] = pd.to_datetime(data.time, dayfirst = True)
data = data.set_index('time')

solar = pd.read_csv('solar.csv')
solar.time = pd.to_datetime(solar.time, dayfirst=True)
solar = solar.set_index('time')
solar = solar.resample('H', label='right', closed='right').mean()
solar.produced_pred['2022-07-27 01:00:00':'2022-07-28 23:00:00'] = np.nan

#%% Make incline radiations from prediction
times    = pd.date_range('2020-01-02 01:00', '2022-12-31 00:00:00', freq='1H', tz='Etc/GMT+0')
loc = location.Location(latitude=-5.044958, longitude=-37.793078, tz=times.tz) 
times = times.tz_localize(None)
# %%solar position
solar_position = solarposition.spa_python(times, latitude=-5.044958, longitude=-37.793078)
# angles
angles = tracking.singleaxis(apparent_zenith = solar_position.apparent_zenith, 
                          apparent_azimuth = solar_position.azimuth, axis_tilt=0, axis_azimuth=0, max_angle=60,
                             backtrack=True, gcr=3.93/14)

# diffuse
difuse = pvlib.irradiance.erbs(ghi = np.array(data.horizontal_pred), zenith = np.array(solar_position.apparent_zenith), 
                               datetime_or_doy = times, min_cos_zenith=0.065, max_zenith=87)

#%% Put all together for incline
poa = pvlib.irradiance.get_total_irradiance(surface_tilt=angles.surface_tilt,
                                              surface_azimuth = angles.surface_azimuth, 
                                              solar_zenith = solar_position.apparent_zenith, 
                                              solar_azimuth = solar_position.azimuth, 
                                              dni = difuse.dni, 
                                              ghi = data.horizontal_pred, 
                                              dhi = difuse.dhi)
poa = poa.fillna(0)
#%% Make module temperature model 
mask = data[~data.isnull().any(axis=1)].index
comb = []
for i in np.arange(4, 8, 0.1):
    for j in np.arange(36, 46,1):
        fill = pvlib.temperature.faiman(data.incline[mask], data.Temperature_pred[mask], 
                                        wind_speed= data.Wind_speed_pred[mask], u0=j, u1=i)
        mse  = mean_squared_error(data.module[mask], fill)
        comb.append([i,j, mse])
        
result = pd.DataFrame(data = comb, columns = ['u1', 'u0', 'mse'])
print(result.iloc[result.mse.idxmin()])
math.sqrt(5.78)

#%% Make module temperature from forecast
module_pred = pvlib.temperature.faiman(poa.poa_global, data.Temperature_pred, wind_speed= data.Wind_speed_pred, u0=42, u1=6.7)
#%% Make DC current
DC = pvlib.pvsystem.pvwatts_dc(g_poa_effective = poa.poa_global, temp_cell = module_pred, pdc0 = 162247800, gamma_pdc = 0.0039 , temp_ref=25.0)
#%% Make AC current 
inverter = {'pdc0':162247800, 'eta_inv' : 0.995}
pdc0 = [162247800]
inv = pvlib.pvsystem.PVSystem(inverter_parameters=inverter).get_ac(model = 'pvwatts', p_dc = DC)  #, eta_inv_nom=0.955)
pdc0 = [162247800]
#inverter = pd.DataFrame(data= [162247800], columns = 'Pnom')
#AC = inv.get_ac(model = 'pvwatts', p_dc = DC)  #, eta_inv_nom=0.955)
#%% add ac to dataframe 
new_time = pd.date_range(start='2020-01-02 01:00:00', end='2022-12-30 23:45:00', freq = '1H')
AC_full = AC.reindex(new_time).reset_index()
AC_full = AC_full.set_index('index')

data['empiric'] = AC_full[0]

data.to_csv('full_w_all_pred.csv')
#%%
def PAPE(truth, pred, horizon):
    tot = 0
    periods = int(len(truth) / horizon)
    indexes = np.linspace(0, periods*horizon-1, periods,
                          dtype=int, endpoint=False)
    for i in indexes:
        truth_peak = max(truth[i:i+horizon])
        pred_peak = max(pred[i:i+horizon])
        if truth_peak != 0:
            tot += abs(truth_peak - pred_peak)/truth_peak
    return float((100/periods)*tot)


#%%

test_split = 20544
maske  = mask_days_with_nan(data[test_split:],solar.produced_pred[test_split:])
y_test = data.produced_fixed[test_split:]
y_test = y_test[maske]*6
y_test = y_test/max(y_test)
y_emp  = DC[test_split:]
y_emp  = y_emp[maske]
y_emp  = y_emp/max(y_emp)
#%%
pape_emp = PAPE(y_test, y_emp, 23)
mse_emp = mean_squared_error(y_test, y_emp)
mbe_emp = np.mean([true - pred for (true, pred) in zip(y_test, y_emp)])

mse_persistence = 0.023121522025697613
ss_emp = 1 - mse_emp / mse_persistence
print('PAPE emp', pape_emp)
print('MSE emp ', mse_emp)
print('RMSE emp ', math.sqrt(mse_emp))
print('MBE emp ', mbe_emp)
print('skill score emp ', ss_emp)
#%%
plt.figure()
plt.scatter(y_test, y_emp, color='purple')

#%%
plt.figure()
y_emp.plot()
y_test.plot()
#%%
plt.figure()
poa.poa_global.plot()
data.incline.plot()
#%%
plt.figure()
module_pred.plot()
data.module.plot()